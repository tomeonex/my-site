from django.shortcuts import render, get_object_or_404, redirect
from .models import Recipe, Category

#def recipes_list(request):
#    recipes = Recipe.objects.order_by('author').reverse()
#    categories = Category.objects.order_by('name')
#    return render(request, 'recipes/recipes_list.html', {'recipes': recipes, 'categories': categories})

def recipe_detail(request, pk):
    recipe = get_object_or_404(Recipe, pk=pk)
    return render(request, 'recipes/recipe_detail.html', {'recipe': recipe})

def recipes_list(request):
    id = 0
    categories = Category.objects.all()
    recipes = Recipe.objects.order_by('author').reverse()
    
    if request.method == "POST":
        if '7' in request.POST:
            return render(request, 'recipes/recipes_list.html', {'recipes': recipes, 'categories': categories})
        
        else:
            for cat in categories:
                id += 1
                check = str(id)
                if check in request.POST:
                    break
            recipes = Recipe.objects.filter(category=id)
            return render(request, 'recipes/recipes_list.html', {'recipes': recipes, 'categories': categories})
    
    else:
        return render(request, 'recipes/recipes_list.html', {'recipes': recipes, 'categories': categories})