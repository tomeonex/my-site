from django.db import models


class Category(models.Model):

    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Recipe(models.Model):
    
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    ingredients = models.TextField()
    text = models.TextField()
    image = models.ImageField(upload_to='images/')
    category = models.ManyToManyField(Category)
    easy = models.BooleanField(default=False)

    def __str__(self):
        return self.title
