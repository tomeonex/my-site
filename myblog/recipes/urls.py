from django.urls import path
from . import views

app_name = 'recipes'
urlpatterns = [
    path('', views.recipes_list, name='recipes_list'),
    path('recipe/<int:pk>', views.recipe_detail, name='recipe_detail'),
]

